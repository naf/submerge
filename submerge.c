#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void populatearray (FILE *inputfile, char sub[][256]) {
	char line[128];
	int subcount = 0;

	while (fgets(line, sizeof(line), inputfile) != NULL) {
		if (subcount + 1 == atoi(line) - 1 ) {
			subcount++;
			continue;
		}
		
		if (line[0] < 13 ) {
			continue;
		}

		if (strstr(line, " --> ") != NULL) {
			for (int i = 0; i < 11 && line[i] != '\0'; ++i) {
				if (line[i] >= 32) {
					switch (line[i]) {
						case ',':
							sub[subcount][i] = '.';
							break;
						case '>':
							sub[subcount][i] = '.';
							break;
						case '-':
							break;
						default:
							sub[subcount][i] = line[i];
					}
				}
			}
			continue;
		}

		int i = 11;
		while (sub[subcount][i] != '\0') {
			i++;
		}

		sub[subcount][i] = ' ';

		for (int j = 0; line[j] != '\0'; j++) {
			if (line[j] >= 32) {
				sub[subcount][i+1] = line[j];
				i++;
			}
		}
	}
}
	
void writeheaders (FILE *outputfile) {
 	fprintf(outputfile,"[Script Info]\n");
 	fprintf(outputfile,"ScriptType: v4.00+\n");
 	fprintf(outputfile,"Collisions: Normal\n");
 	fprintf(outputfile,"\n");
 	fprintf(outputfile,"[V4+ Styles]\n");
 	fprintf(outputfile,"Format: Name, Fontsize, PrimaryColour, OutlineColour, MarginV, MarginL, MarginR, BorderStyle,Outline,Shadow,Alignment\n");
 	fprintf(outputfile,"Style: one, 12, &HFFFFFF, &H154e62,  35,10,10,1,1,0,1\n");
 	fprintf(outputfile,"Style: two, 12, &HFFFFFF, &H467c48, 5,10,10,1,1,0,1\n");
 	fprintf(outputfile,"\n");
 	fprintf(outputfile,"[Events]\n");
 	fprintf(outputfile,"Format: Style, Start, End, Text\n");
}

void writesubs (FILE *outputfile, char sub[][256], const char *style) {
	for (int i = 0; sub[i][0] != '\0'; i++) {
		
		fprintf(outputfile,"Dialogue: %s , ", style);

		// write start time
		for (int j = 0; j < 11 && sub[i][j] != '\0'; j++) {
			fprintf(outputfile,"%c", sub[i][j]);
		}
		
		fprintf(outputfile," , ");

		// write end time
		for (int j = 0; j < 11 && sub[i+1][j] != '\0'; j++) {
			fprintf(outputfile,"%c", sub[i+1][j]);
		}

		if (sub[i+1][0] == '\0') {
				fprintf(outputfile,"99:99:99.99");
		}
		
		fprintf(outputfile," , ");

		// write subtitle
		for (int j = 12; j < 256 && sub[i][j] != '\0'; j++) {
			fprintf(outputfile,"%c", sub[i][j]);
		}
		fprintf(outputfile,"\n");
	}
}

int main (int argc, char *argv[]) {
	
	if (argc != 3) {
		fprintf(stderr, "usage: %s <primary_sub> <secondary_sub>\n", argv[0]);
		return 1;
	}
	
	FILE *output = fopen("subtitle.ass", "w");
	FILE *input1 = fopen(argv[1],"r");
	FILE *input2 = fopen(argv[2],"r");

	if (output == NULL || input1 == NULL || input2 == NULL) {
		perror("Error opening file");
		return 1;
	}

	char sub[2000][256];
	
	writeheaders(output);

	populatearray(input1, sub);
	fclose(input1);	
	writesubs(output, sub, "one");

	memset(sub, '\0', sizeof(sub));

	populatearray(input2, sub);
	fclose(input2);	
	writesubs(output, sub, "two");

	fclose(output);
	return 0;
}
